export interface IGameScreen {
  width: number;
  height: number;
}

export class CourtSide {
  public static LEFT = 0;
  public static RIGHT = 1;
}

export interface IVector2 {
  x: number;
  y: number;
}

export interface IBall {
  position: IVector2;
  radius: number;
  readonly speed: number;
}

export interface IPaddle {
  position: IVector2;
  readonly speed: number;
  readonly height: number;
  readonly width: number;
}

export interface IPlayer {
  side: number;
  paddle: IPaddle;
}

export declare type Scoreboard = [number, number];

// Event Emit Types

export interface EventGameStartPayload {
  screen: IGameScreen;
  ball: IBall;
  paddles: [IPaddle, IPaddle];
  player: IPlayer;
}

export interface EventScoreUpdatedPayload {
  scoreboard: Scoreboard;
}

export type EventGameEndPayload = EventScoreUpdatedPayload;

export interface EventPaddleStartMovingPayload {
  direction: IVector2;
}

export interface EventGameLoopUpdatePayload {
  ball: IBall;
  paddles: [IPaddle, IPaddle];
}
