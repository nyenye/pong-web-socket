import * as Express from "express";
import * as HTTP from "http";
import * as SocketIO from "socket.io";

import { PongGame } from "@pongws/pong";
import { IGameScreen } from "@pongws/types";

import { E_PLAYER_READY, E_USER_HAS_CONNECTED } from "@pongws/ws-messaging";

const playerQueue: Array<SocketIO.Socket> = [];
const games: Array<PongGame> = [];

const canStartGame = (): boolean =>
  playerQueue.length >= 2 && playerQueue.length % 2 === 0; // && games.length < 10;

const startGame = (left: SocketIO.Socket, right: SocketIO.Socket): void => {
  const screen: IGameScreen = { width: 1920, height: 1080 };

  const game = new PongGame([left, right], screen, onGameFinished);

  games.push(game);
};

const onGameFinished = (gameToRemove: PongGame) => {
  const indexToRemove = games.indexOf(gameToRemove);
  games.splice(indexToRemove, 1);
};

const fps = 15;

const loop = (delta: number) => {
  games.forEach((game: PongGame) => {
    game.update(delta);
  });
};

const init = async () => {
  const app = Express();

  const httpServer = HTTP.createServer(app);

  const io = SocketIO(httpServer, { origins: "*:*" });

  io.on("connection", (socket: SocketIO.Socket) => {
    socket.on(E_PLAYER_READY, () => {
      playerQueue.push(socket);

      if (canStartGame()) {
        startGame(playerQueue.shift(), playerQueue.shift());
      }
    });

    io.emit(E_USER_HAS_CONNECTED);
  });

  const port: number = 3000;
  try {
    await httpServer.listen(port);
    console.log(`SUCCESS - Server listening on port ${port}`);

    setInterval(loop, 1000 / fps, 1 / fps);
  } catch (err) {
    console.log(err);
    console.log("ERROR - Failed to bring up server");
  }
};

init();
