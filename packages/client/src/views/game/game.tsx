// Vue
import { createComponent } from "@vue/composition-api";
// Hooks
import {
  useStore,
  useSetupPongGame,
  useGame,
  useSetupInputHandler,
  useDisplaySize
} from "@/hooks";
// Types
import { HookUseDisplaySize, HookUseGameReturn } from "@/types";
// CSS
import styles from "./styles.module.scss";
// Components
import { Ball, Scoreboard } from "@/components";
import { E_SCOREBOARD_UPDATED } from "@pongws/ws-messaging";

interface GameViewProps {}

const GameView = createComponent<GameViewProps, {}>({
  props: {},
  setup(props, context) {
    const store = useStore();

    const {
      isReady,
      isRunning,
      ball,
      paddles,
      player,
      screen,
      scoreboard
    }: HookUseGameReturn = useGame(store);

    const { aspectRatio, gameSize }: HookUseDisplaySize = useDisplaySize(
      screen.value
    );

    useSetupPongGame(store);

    useSetupInputHandler(store);

    return () => {
      if (!isReady.value) {
        if (scoreboard.value[0] > 0 || scoreboard.value[1] > 0) {
          const isWinner =
            scoreboard.value[player.value.side] >
            scoreboard.value[(player.value.side + 1) % 2];

          return (
            <div id="game" class={styles["game-not-ready"]}>
              <div id="scoreboard" class="scoreboard">
                {scoreboard.value[0]} - {scoreboard.value[1]}
              </div>

              {(isWinner && <h2>You Win :)</h2>) || <h2>You Lost :(</h2>}

              <div class="play-again">Press Enter to join another game</div>
            </div>
          );
        }

        return (
          <div class={styles["game-not-ready"]}>Press Enter to join a game</div>
        );
      }

      if (!isRunning.value) {
        return (
          <div id="game" class={styles["game-not-ready"]}>
            Awaiting other player
          </div>
        );
      }

      const ballStyle = `
        width: ${(ball.value.radius * 2) / aspectRatio.value}px;
        height: ${(ball.value.radius * 2) / aspectRatio.value}px;
        left: calc(${(ball.value.position.x / screen.value.width) *
          100}% - ${(ball.value.radius / screen.value.width) * 100}%);
        top: ${(ball.value.position.y / screen.value.height) * 100}%;
      `;

      const leftPaddleStyle = `
        width: ${(paddles.value[0].width / screen.value.width) * 100}%;
        height: ${(paddles.value[0].height / screen.value.height) * 100}%;
        left: ${(paddles.value[0].position.x / screen.value.width) * 100}%;
        top: ${(paddles.value[0].position.y / screen.value.height) * 100}%;
      `;

      const rightPaddleStyle = `
        width: ${(paddles.value[1].width / screen.value.width) * 100}%;
        height: ${(paddles.value[1].height / screen.value.height) * 100}%;
        left: ${(paddles.value[1].position.x / screen.value.width) * 100}%;
        top: ${(paddles.value[1].position.y / screen.value.height) * 100}%;
      `;

      return (
        <div
          id="game"
          class={styles["game"]}
          style={`width: ${gameSize.value.width}px; height: ${gameSize.value.height}px`}
        >
          <div id="scoreboard" class="scoreboard">
            {scoreboard.value[0]} - {scoreboard.value[1]}
          </div>

          <div id="divider" class="divider"></div>

          <div
            id="left-paddle"
            class="pong-paddle"
            style={leftPaddleStyle}
          ></div>

          <div id="ball" class="pong-ball" style={ballStyle}></div>

          <div
            id="right-paddle"
            class="pong-paddle"
            style={rightPaddleStyle}
          ></div>
        </div>
      );
    };
  }
});

export default GameView;

export { GameView };
