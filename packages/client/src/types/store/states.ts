import {
  IGameScreen,
  IBall,
  IPaddle,
  IPlayer,
  Scoreboard
} from "@pongws/types";

export interface GameState {
  isReady: boolean;
  isRunning: boolean;
  screen: IGameScreen;
  ball: IBall;
  paddles: [IPaddle, IPaddle];
  player: IPlayer;
  scoreboard: Scoreboard;
}

export interface RootState {
  game: GameState;
}
