import { Ref } from "@vue/composition-api";
import { IPlayer, IPaddle, IBall, IGameScreen } from "@pongws/types";

export interface HookUseGameReturn {
  isReady: Readonly<Ref<boolean>>;
  isRunning: Readonly<Ref<boolean>>;
  player: Readonly<Ref<Readonly<IPlayer>>>;
  ball: Readonly<Ref<Readonly<IBall>>>;
  paddles: Readonly<Ref<readonly [IPaddle, IPaddle]>>;
  screen: Readonly<Ref<Readonly<IGameScreen>>>;
  scoreboard: Readonly<Ref<readonly [number, number]>>;
}

export interface HookUseDisplaySize {
  wrapperSize: Readonly<Ref<Readonly<ClientRect>>>;
  aspectRatio: Readonly<Ref<number>>;
  gameSize: Readonly<Ref<Readonly<IGameScreen>>>;
}
