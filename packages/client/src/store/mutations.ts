export const M_SET_IS_READY = "M_SET_IS_READY";

export const M_SET_GAME_START = "M_SET_GAME_START";

export const M_SET_GAME_LOOP_UPDATE = "M_SET_GAME_LOOP_UPDATE";

export const M_SET_SCOREBOARD = "M_SET_SCOREBOARD";

export const M_SET_GAME_END = "M_SET_GAME_END";
