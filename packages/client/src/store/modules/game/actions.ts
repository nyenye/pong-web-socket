// Vuex
import { ActionTree, ActionHandler, ActionContext } from "vuex";
// Types
import {
  EventGameStartPayload,
  EventGameLoopUpdatePayload,
  EventScoreUpdatedPayload,
  EventGameEndPayload,
  EventPaddleStartMovingPayload
} from "@pongws/types";
import { GameState, RootState } from "@/types";
// Actions
import {
  A_JOIN_GAME,
  A_GAME_START,
  A_GAME_LOOP_UPDATE,
  A_UPDATE_SCOREBOARD,
  A_GAME_END,
  A_STOP_MOVING_PADDLE,
  A_START_MOVING_PADDLE
} from "@/store/actions";
// Mutations
import {
  M_SET_IS_READY,
  M_SET_GAME_START,
  M_SET_GAME_LOOP_UPDATE,
  M_SET_SCOREBOARD,
  M_SET_GAME_END
} from "@/store/mutations";
// Socket
import { Socket } from "@/sockets";
// Events
import {
  E_PLAYER_READY,
  E_PADDLE_START_MOVING,
  E_PADDLE_STOP_MOVING
} from "@pongws/ws-messaging";

const joinGame: ActionHandler<GameState, RootState> = (
  context: ActionContext<GameState, RootState>
) => {
  const { commit } = context;
  Socket.emit(E_PLAYER_READY);
  commit(M_SET_IS_READY);
};

const startGame: ActionHandler<GameState, RootState> = (
  context: ActionContext<GameState, RootState>,
  payload: EventGameStartPayload
) => {
  const { commit } = context;
  commit(M_SET_GAME_START, payload, { root: false });
};

const startMovingPaddle: ActionHandler<GameState, RootState> = (
  context: ActionContext<GameState, RootState>,
  payload: EventPaddleStartMovingPayload
) => {
  Socket.emit(E_PADDLE_START_MOVING, payload);
};

const stopMovingPaddle: ActionHandler<GameState, RootState> = (
  context: ActionContext<GameState, RootState>,
  payload: EventGameStartPayload
) => {
  Socket.emit(E_PADDLE_STOP_MOVING);
};

const gameLoopUpdate: ActionHandler<GameState, RootState> = (
  context: ActionContext<GameState, RootState>,
  payload: EventGameLoopUpdatePayload
) => {
  const { commit } = context;
  commit(M_SET_GAME_LOOP_UPDATE, payload, { root: false });
};

const scoreboardUpdate: ActionHandler<GameState, RootState> = (
  context: ActionContext<GameState, RootState>,
  payload: EventScoreUpdatedPayload
) => {
  const { commit } = context;
  commit(M_SET_SCOREBOARD, payload, { root: false });
};

const endGame: ActionHandler<GameState, RootState> = (
  context: ActionContext<GameState, RootState>,
  payload: EventGameEndPayload
) => {
  const { commit } = context;
  commit(M_SET_GAME_END, payload, { root: false });
};

const actions: ActionTree<GameState, RootState> = {
  [A_JOIN_GAME]: joinGame,
  [A_GAME_START]: startGame,
  [A_START_MOVING_PADDLE]: startMovingPaddle,
  [A_STOP_MOVING_PADDLE]: stopMovingPaddle,
  [A_GAME_LOOP_UPDATE]: gameLoopUpdate,
  [A_UPDATE_SCOREBOARD]: scoreboardUpdate,
  [A_GAME_END]: endGame
};

export default actions;
