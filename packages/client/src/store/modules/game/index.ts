import { Module } from "vuex";
import { GameState, RootState } from "@/types";

import state from "@/store/modules/game/state";
import getters from "@/store/modules/game/getters";
import actions from "@/store/modules/game/actions";
import mutations from "@/store/modules/game/mutations";

const module: Module<GameState, RootState> = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};

export default module;
