import { CourtSide } from "@pongws/types";
import { GameState } from "@/types";

const state: GameState = {
  isReady: false,
  isRunning: false,
  screen: { width: 720, height: 405 },
  scoreboard: [0, 0],
  ball: {
    position: { x: 0, y: 0 },
    radius: 20,
    speed: 0
  },
  paddles: [
    {
      position: { x: 0, y: 0 },
      height: 100,
      width: 10,
      speed: 50
    },
    {
      position: { x: 0, y: 0 },
      height: 100,
      width: 10,
      speed: 50
    }
  ],
  player: {
    paddle: {
      position: { x: 0, y: 0 },
      height: 100,
      width: 10,
      speed: 50
    },
    side: 0
  }
};

export default state;
