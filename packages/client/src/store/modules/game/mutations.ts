// Vuex
import { MutationTree, Mutation } from "vuex";
// Types
import {
  EventGameStartPayload,
  EventGameEndPayload,
  EventGameLoopUpdatePayload,
  EventScoreUpdatedPayload
} from "@pongws/types";
import { GameState } from "@/types";
// Mutations
import {
  M_SET_IS_READY,
  M_SET_GAME_START,
  M_SET_GAME_LOOP_UPDATE,
  M_SET_GAME_END,
  M_SET_SCOREBOARD
} from "@/store/mutations";

const setIsReady: Mutation<GameState> = (
  state: GameState,
  payload: EventGameStartPayload
) => {
  state.isReady = true;
};

const setGameStart: Mutation<GameState> = (
  state: GameState,
  payload: EventGameStartPayload
) => {
  state.screen = payload.screen;
  state.ball = payload.ball;
  state.paddles = payload.paddles;
  state.player = payload.player;
  state.scoreboard = [0, 0];
  state.isRunning = true;
};

const setGameLoopUpdate: Mutation<GameState> = (
  state: GameState,
  payload: EventGameLoopUpdatePayload
) => {
  state.ball = payload.ball;
  state.paddles = payload.paddles;
};

const setScoreboard: Mutation<GameState> = (
  state: GameState,
  payload: EventScoreUpdatedPayload
) => {
  state.scoreboard = payload.scoreboard;
};

const setGameEnd: Mutation<GameState> = (
  state: GameState,
  payload: EventGameEndPayload
) => {
  state.scoreboard = payload.scoreboard;
  state.isRunning = false;
  state.isReady = false;
};

const mutations: MutationTree<GameState> = {
  [M_SET_IS_READY]: setIsReady,
  [M_SET_GAME_START]: setGameStart,
  [M_SET_GAME_LOOP_UPDATE]: setGameLoopUpdate,
  [M_SET_SCOREBOARD]: setScoreboard,
  [M_SET_GAME_END]: setGameEnd
};

export default mutations;
