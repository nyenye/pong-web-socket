import { GetterTree, Getter } from "vuex";
import { GameState, RootState } from "@/types";

const getters: GetterTree<GameState, RootState> = {};

export default getters;
