// Vuex
import { ModuleTree } from "vuex";
// Types
import { RootState } from "@/types";
// Namespaces
import { NS_GAME } from "@/store/namespaces";
// Modules
import GameModule from "@/store/modules/game";

const rootModules: ModuleTree<RootState> = {
  [NS_GAME]: GameModule
};

export default rootModules;
