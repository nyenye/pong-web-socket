// CompositionApi
import { computed, reactive, ref, onMounted } from "@vue/composition-api";
// Types
import { MyStore, HookUseGameReturn, HookUseDisplaySize } from "@/types";
// Socket
import { setupCallbacks } from "@/sockets";
import { NS_GAME } from "@/store/namespaces";
import {
  A_JOIN_GAME,
  A_START_MOVING_PADDLE,
  A_STOP_MOVING_PADDLE
} from "@/store/actions";
import { IGameScreen } from "@pongws/types";

export const useGame = (store: MyStore): HookUseGameReturn => {
  const isReady = computed(() => store.state.game.isReady);

  const isRunning = computed(() => store.state.game.isRunning);

  const paddles = computed(() => store.state.game.paddles);

  const ball = computed(() => store.state.game.ball);

  const screen = computed(() => store.state.game.screen);

  const player = computed(() => store.state.game.player);

  const scoreboard = computed(() => store.state.game.scoreboard);

  return { isReady, isRunning, player, ball, paddles, screen, scoreboard };
};

export const useDisplaySize = (serverSize: IGameScreen): HookUseDisplaySize => {
  const wrapperSize = computed(() => {
    const appElement: HTMLElement | null = document.getElementById("app");

    // if (appElement!.getBoundingClientRect().width > serverSize.width) {
    //   return {
    //     width: serverSize.width,
    //     height: serverSize.height
    //   } as IGameScreen;
    // }

    return appElement!.getBoundingClientRect() as ClientRect;
  });

  const aspectRatio = computed(() => {
    const aspectRatioW = serverSize.width / wrapperSize.value.width;
    const aspectRatioH = serverSize.height / wrapperSize.value.height;

    return aspectRatioW < aspectRatioH ? aspectRatioH : aspectRatioW;
  });

  const gameSize = computed(() => {
    return {
      width: serverSize.width / aspectRatio.value,
      height: serverSize.height / aspectRatio.value
    } as IGameScreen;
  });

  return { wrapperSize, aspectRatio, gameSize };
};

export const useSetupInputHandler = (store: MyStore) => {
  onMounted(() => {
    addEventListener("keydown", (event: KeyboardEvent) => {
      if (!store.state.game.isReady || !store.state.game.isRunning) {
        return;
      }

      switch (event.key) {
        case "ArrowUp":
          store.dispatch(`${NS_GAME}/${A_START_MOVING_PADDLE}`, {
            direction: { x: 0, y: -1 }
          });
          return;
        case "ArrowDown":
          store.dispatch(`${NS_GAME}/${A_START_MOVING_PADDLE}`, {
            direction: { x: 0, y: 1 }
          });
          return;
      }
    });

    addEventListener("keyup", (event: KeyboardEvent) => {
      if (!store.state.game.isReady) {
        if (event.key === "Enter") {
          store.dispatch(`${NS_GAME}/${A_JOIN_GAME}`);
        }
        return;
      }

      if (!store.state.game.isRunning) {
        return;
      }

      // If game is running:
      switch (event.key) {
        case "ArrowUp":
          store.dispatch(`${NS_GAME}/${A_STOP_MOVING_PADDLE}`);
          return;
        case "ArrowDown":
          store.dispatch(`${NS_GAME}/${A_STOP_MOVING_PADDLE}`);
          return;
      }
    });
  });
};

export const useSetupPongGame = (store: MyStore) => {
  onMounted(() => {
    setupCallbacks(store);
  });
};
