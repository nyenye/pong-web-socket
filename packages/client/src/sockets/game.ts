// Socket
import { Socket } from "@/sockets";
// Events
import {
  E_GAME_START,
  E_GAME_LOOP_UPDATE,
  E_SCOREBOARD_UPDATED,
  E_GAME_END
} from "@pongws/ws-messaging";
// Types
import {
  EventGameStartPayload,
  EventGameEndPayload,
  EventGameLoopUpdatePayload,
  EventScoreUpdatedPayload
} from "@pongws/types";
import { MyStore } from "@/types";
// Actions
import { NS_GAME } from "@/store/namespaces";
import {
  A_GAME_START,
  A_GAME_LOOP_UPDATE,
  A_UPDATE_SCOREBOARD,
  A_GAME_END
} from "@/store/actions";

export const setupCallbacks = (store: MyStore): void => {
  Socket.on(E_GAME_START, (payload: EventGameStartPayload) => {
    store.dispatch(`${NS_GAME}/${A_GAME_START}`, payload);
  });

  Socket.on(E_GAME_LOOP_UPDATE, (payload: EventGameLoopUpdatePayload) => {
    store.dispatch(`${NS_GAME}/${A_GAME_LOOP_UPDATE}`, payload);
  });

  Socket.on(E_SCOREBOARD_UPDATED, (payload: EventScoreUpdatedPayload) => {
    store.dispatch(`${NS_GAME}/${A_UPDATE_SCOREBOARD}`, payload);
  });

  Socket.on(E_GAME_END, (payload: EventGameEndPayload) => {
    store.dispatch(`${NS_GAME}/${A_GAME_END}`, payload);
  });
};
