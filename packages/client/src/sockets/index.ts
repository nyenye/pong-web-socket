import io from "socket.io-client";

const Socket = io("http://localhost:3000");

export { Socket };

export * from "@/sockets/game";
