// Vue
import { createComponent, computed, onMounted } from "@vue/composition-api";
// Types
import { IBall, IGameScreen } from "@pongws/types";
// CSS
import styles from "./styles.module.scss";

export interface BallProps {
  ball: IBall;
  aspectRatio: number;
  screen: IGameScreen;
}

export const Ball = createComponent<BallProps, {}>({
  props: {},
  setup(props: Readonly<BallProps>) {
    const { ball, screen, aspectRatio } = props;

    return () => {
      const ballStyle = `
        width: ${(ball.radius * 2) / aspectRatio}px;
        height: ${(ball.radius * 2) / aspectRatio}px;
        left: ${(ball.position.x / screen.width) * 100}%;
        top: ${(ball.position.y / screen.height) * 100}%;
      `;

      <div id="ball" class="pong-ball" style={ballStyle}></div>;
    };
  }
});
