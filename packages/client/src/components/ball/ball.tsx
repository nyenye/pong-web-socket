// Vue
import { createComponent, computed, onMounted } from "@vue/composition-api";
// Types
import { IBall, IGameScreen } from "@pongws/types";
// CSS
import styles from "./styles.module.scss";

export interface BallProps {
  ball: IBall;
  screen: IGameScreen;
}

export const Ball = createComponent<BallProps, {}>({
  props: {
    ball: {
      required: true
    },
    screen: {
      required: true
    }
  },
  setup(props: Readonly<BallProps>) {
    const { ball, screen } = props;

    return () => {
      const ballStyle = `
        width: ${((ball.radius * 2) / screen.width) * 100}%;
        height: ${((ball.radius * 2) / screen.width) * 100}%;
        left: ${(ball.position.x / screen.width) * 100}%;
        top: ${(ball.position.y / screen.height) * 100}%;
      `;

      return (
        <div id="ball" class={styles["pong-ball"]} style={ballStyle}></div>
      );
    };
  }
});
