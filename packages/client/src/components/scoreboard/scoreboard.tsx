// Vue
import { createComponent, computed, onMounted } from "@vue/composition-api";
// Types
import { Scoreboard as IScoreboard } from "@pongws/types";
// CSS
import styles from "./styles.module.scss";

export interface ScoreboardProps {
  scoreboard: IScoreboard;
}

export const Scoreboard = createComponent<ScoreboardProps, {}>({
  props: {
    scoreboard: { required: true }
  },
  setup(props: Readonly<ScoreboardProps>) {
    const { scoreboard } = props;

    return () => {
      return (
        <div id="scoreboard" class={styles["scoreboard"]}>
          [{scoreboard[0]} - {scoreboard[1]}]
        </div>
      );
    };
  }
});
