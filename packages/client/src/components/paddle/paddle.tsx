// Vue
import { createComponent, computed, onMounted } from "@vue/composition-api";
// Types
import { IPaddle, IGameScreen, CourtSide } from "@pongws/types";
// CSS
import styles from "./styles.module.scss";

export interface PaddleProps {
  paddle: IPaddle;
  side: number;
  screen: IGameScreen;
}

export const Paddle = createComponent<PaddleProps, {}>({
  props: {
    paddle: {
      required: true
    },
    side: {
      required: true
    },
    screen: {
      required: true
    }
  },
  setup(props: Readonly<PaddleProps>) {
    const { paddle, side, screen } = props;

    return () => {
      const paddleStyle = `
        width: ${(paddle.width / screen.width) * 100}%;
        height: ${(paddle.height / screen.height) * 100}%;
        left: ${(paddle.position.x / screen.width) * 100}%;
        top: ${(paddle.position.y / screen.height) * 100}%;
      `;

      return (
        <div
          side={side == CourtSide.LEFT ? "left" : "right"}
          class={styles["pong-paddle"]}
          style={paddleStyle}
        ></div>
      );
    };
  }
});
