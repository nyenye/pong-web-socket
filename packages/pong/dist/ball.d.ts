import { Socket } from "socket.io";
import { IBall, IVector2, IGameScreen, IPaddle } from "@pongws/types";
export declare class Ball implements IBall {
    private sockets;
    position: IVector2;
    radius: number;
    speed: number;
    private direction;
    constructor(screen: IGameScreen, sockets: Array<Socket>);
    toIBall(): IBall;
    update(delta: number): void;
    hasCollidedAABB(paddle: IPaddle): boolean;
    rebound(scaleX: number, scaleY: number, increaseSpeed?: boolean): void;
    reset(screen: IGameScreen, direction: IVector2): void;
}
