"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Types
const types_1 = require("@pongws/types");
const ball_1 = require("./ball");
const player_1 = require("./player");
// Events
const ws_messaging_1 = require("@pongws/ws-messaging");
class PongGame {
    constructor(sockets, screen, onFinished) {
        this.sockets = [];
        this.isOver = false;
        if (sockets.length !== 2) {
            throw Error("Could not initialize game. Must have exactly 2 players");
        }
        this.sockets = sockets;
        this.screen = screen;
        this.ball = new ball_1.Ball(screen, sockets);
        this.players = this.sockets.map((socket, index) => {
            const side = index === 0 ? types_1.CourtSide.LEFT : types_1.CourtSide.RIGHT;
            const paddle = {
                position: {
                    x: side === types_1.CourtSide.LEFT ? 10 : screen.width - 20,
                    y: screen.height / 2 - 25
                },
                height: 100,
                width: 10,
                speed: 300
            };
            return new player_1.Player(socket, paddle, side, screen);
        });
        this.onFinished = onFinished;
        sockets.forEach((socket, index) => {
            const eventGameStartPayload = {
                screen,
                ball: this.ball.toIBall(),
                paddles: this.paddles,
                player: this.players[index].toIPlayer()
            };
            socket.emit(ws_messaging_1.E_GAME_START, eventGameStartPayload);
            const eventGameLoopUpdatePayload = {
                ball: this.ball.toIBall(),
                paddles: this.paddles
            };
        });
        this.update = this.update.bind(this);
        this.broadcast = this.broadcast.bind(this);
        this.score = this.score.bind(this);
    }
    get scoreboard() {
        return [this.players[0].score, this.players[1].score];
    }
    get paddles() {
        return [this.players[0].paddle, this.players[1].paddle];
    }
    update(delta) {
        if (this.isOver)
            return;
        this.ball.update(delta);
        this.players.forEach((player) => player.update(delta));
        const eventGameLoopUpdatePayload = {
            ball: this.ball.toIBall(),
            paddles: this.paddles
        };
        this.broadcast(ws_messaging_1.E_GAME_LOOP_UPDATE, eventGameLoopUpdatePayload);
        if (this.ball.hasCollidedAABB(this.players[0].paddle) ||
            this.ball.hasCollidedAABB(this.players[1].paddle)) {
            this.ball.rebound(-1, 1, true);
        }
        else if (this.ball.position.y <= 0 ||
            this.ball.position.y >= this.screen.height - this.ball.radius * 2) {
            this.ball.rebound(1, -1);
        }
        if (this.ball.position.x <= 0) {
            this.score(this.players[1]);
            this.ball.reset(this.screen, { x: 1, y: -1 });
        }
        else if (this.ball.position.x >=
            this.screen.width - this.ball.radius * 2) {
            this.score(this.players[0]);
            this.ball.reset(this.screen, { x: -1, y: 1 });
        }
        const scoreboard = this.scoreboard;
        if (scoreboard[0] >= 2 || scoreboard[1] >= 2) {
            this.isOver = true;
            const eventGameEndPayload = {
                scoreboard: this.scoreboard
            };
            this.broadcast(ws_messaging_1.E_GAME_END, eventGameEndPayload);
            this.onFinished(this);
        }
    }
    broadcast(event, payload) {
        this.sockets.forEach((socket) => {
            socket.emit(event, payload);
        });
    }
    score(player) {
        player.score += 1;
        const eventScoreUpdatedPayload = {
            scoreboard: this.scoreboard
        };
        this.sockets.forEach((socket) => {
            socket.emit(ws_messaging_1.E_SCOREBOARD_UPDATED, eventScoreUpdatedPayload);
        });
    }
}
exports.default = PongGame;
exports.PongGame = PongGame;
