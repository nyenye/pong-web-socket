"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vector2_1 = require("./vector2");
class Ball {
    constructor(screen, sockets) {
        this.direction = { x: 0, y: 0 };
        this.sockets = sockets;
        this.radius = 5;
        this.position = {
            x: screen.width / 2 - this.radius,
            y: screen.height / 2 - this.radius
        };
        this.speed = 300;
        this.toIBall = this.toIBall.bind(this);
        this.update = this.update.bind(this);
        this.hasCollidedAABB = this.hasCollidedAABB.bind(this);
        this.rebound = this.rebound.bind(this);
        this.reset = this.reset.bind(this);
        this.reset(screen, { x: -1, y: 1 });
    }
    toIBall() {
        return { position: this.position, radius: this.radius, speed: this.speed };
    }
    update(delta) {
        const movement = vector2_1.Vector2.scale(vector2_1.Vector2.normalized(this.direction), this.speed * delta);
        if (vector2_1.Vector2.magnitude(movement) > 0) {
            this.position = vector2_1.Vector2.add(this.position, movement);
        }
    }
    hasCollidedAABB(paddle) {
        const ax = this.position.x, ay = this.position.y, aw = this.radius * 2, ah = this.radius * 2;
        const bx = paddle.position.x, by = paddle.position.y, bw = paddle.width, bh = paddle.height;
        return ax < bx + bw && ax + aw > bx && ay < by + bh && ay + ah > by;
    }
    rebound(scaleX, scaleY, increaseSpeed = false) {
        this.direction.x *= scaleX;
        this.direction.y *= scaleY;
        if (increaseSpeed) {
            this.speed += 10;
        }
    }
    reset(screen, direction) {
        this.speed = 300;
        this.position = {
            x: screen.width / 2 - this.radius,
            y: screen.height / 2 - this.radius
        };
        this.direction = { x: 0, y: 0 };
        setTimeout(() => (this.direction = direction), 2000);
    }
}
exports.Ball = Ball;
