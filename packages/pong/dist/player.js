"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Events
const ws_messaging_1 = require("@pongws/ws-messaging");
// Classes
const vector2_1 = require("./vector2");
class Player {
    constructor(socket, paddle, side, screen) {
        this.score = 0;
        this.isMoving = false;
        this.direction = { x: 0, y: 0 };
        this.socket = socket;
        this.paddle = paddle;
        this.side = side;
        this.screen = screen;
        this.toIPlayer = this.toIPlayer.bind(this);
        this.update = this.update.bind(this);
        this.startMoving = this.startMoving.bind(this);
        this.stopMoving = this.stopMoving.bind(this);
        this.socket.on(ws_messaging_1.E_PADDLE_START_MOVING, (payload) => this.startMoving(payload.direction));
        this.socket.on(ws_messaging_1.E_PADDLE_STOP_MOVING, () => this.stopMoving());
    }
    toIPlayer() {
        return { side: this.side, paddle: this.paddle };
    }
    update(delta) {
        if (!this.isMoving)
            return;
        const movement = vector2_1.Vector2.scale(vector2_1.Vector2.normalized(this.direction), this.paddle.speed * delta);
        if (vector2_1.Vector2.magnitude(movement) > 0) {
            this.paddle.position = vector2_1.Vector2.add(this.paddle.position, movement);
            if (this.paddle.position.y <= 0) {
                this.paddle.position.y = 0;
            }
            else if (this.paddle.position.y + this.paddle.height >=
                this.screen.height) {
                this.paddle.position.y = this.screen.height - this.paddle.height;
            }
        }
    }
    startMoving(dir) {
        this.isMoving = true;
        this.direction = dir;
    }
    stopMoving() {
        this.isMoving = false;
    }
}
exports.Player = Player;
