"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Vector2 {
    static add(a, b) {
        return { x: a.x + b.x, y: a.y + b.y };
    }
    static sub(a, b) {
        return { x: a.x - b.x, y: a.y - b.y };
    }
    static magnitude(vec2) {
        return Math.sqrt(vec2.x * vec2.x + vec2.y * vec2.y);
    }
    static normalized(vec2) {
        const magnitude = Vector2.magnitude(vec2);
        if (magnitude === 0)
            return { x: 0, y: 0 };
        return { x: vec2.x / magnitude, y: vec2.y / magnitude };
    }
    static distance(origin, target) {
        return { x: origin.x - target.x, y: origin.y - target.y };
    }
    static scale(vec2, scalar) {
        return { x: vec2.x * scalar, y: vec2.y * scalar };
    }
}
exports.Vector2 = Vector2;
