import { Socket } from "socket.io";
import { IGameScreen, IPaddle, Scoreboard } from "@pongws/types";
import { Ball } from "./ball";
import { Player } from "./player";
export default class PongGame {
    private sockets;
    private screen;
    ball: Ball;
    players: [Player, Player];
    isOver: boolean;
    private onFinished;
    constructor(sockets: Array<Socket>, screen: IGameScreen, onFinished: Function);
    get scoreboard(): Scoreboard;
    get paddles(): [IPaddle, IPaddle];
    update(delta: number): void;
    private broadcast;
    private score;
}
export { PongGame };
