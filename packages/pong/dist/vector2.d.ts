import { IVector2 } from "@pongws/types";
export declare class Vector2 {
    static add(a: IVector2, b: IVector2): IVector2;
    static sub(a: IVector2, b: IVector2): IVector2;
    static magnitude(vec2: IVector2): number;
    static normalized(vec2: IVector2): IVector2;
    static distance(origin: IVector2, target: IVector2): IVector2;
    static scale(vec2: IVector2, scalar: number): IVector2;
}
