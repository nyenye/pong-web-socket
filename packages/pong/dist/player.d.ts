import { Socket } from "socket.io";
import { IPaddle, IVector2, IPlayer, IGameScreen } from "@pongws/types";
export declare class Player implements IPlayer {
    private socket;
    side: number;
    score: number;
    paddle: IPaddle;
    private screen;
    private isMoving;
    private direction;
    constructor(socket: Socket, paddle: IPaddle, side: number, screen: IGameScreen);
    toIPlayer(): IPlayer;
    update(delta: number): void;
    startMoving(dir: IVector2): void;
    stopMoving(): void;
}
