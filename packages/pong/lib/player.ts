import { Socket } from "socket.io";
// Types
import {
  IPaddle,
  IVector2,
  IPlayer,
  EventPaddleStartMovingPayload,
  IGameScreen
} from "@pongws/types";
// Events
import {
  E_PADDLE_START_MOVING,
  E_PADDLE_STOP_MOVING
} from "@pongws/ws-messaging";
// Classes
import { Vector2 } from "./vector2";

export class Player implements IPlayer {
  private socket: Socket;

  public side: number;

  public score: number = 0;

  public paddle: IPaddle;

  private screen: IGameScreen;

  private isMoving: boolean = false;

  private direction: IVector2 = { x: 0, y: 0 };

  constructor(
    socket: Socket,
    paddle: IPaddle,
    side: number,
    screen: IGameScreen
  ) {
    this.socket = socket;
    this.paddle = paddle;
    this.side = side;
    this.screen = screen;

    this.toIPlayer = this.toIPlayer.bind(this);
    this.update = this.update.bind(this);
    this.startMoving = this.startMoving.bind(this);
    this.stopMoving = this.stopMoving.bind(this);

    this.socket.on(
      E_PADDLE_START_MOVING,
      (payload: EventPaddleStartMovingPayload) =>
        this.startMoving(payload.direction)
    );
    this.socket.on(E_PADDLE_STOP_MOVING, () => this.stopMoving());
  }

  public toIPlayer(): IPlayer {
    return { side: this.side, paddle: this.paddle };
  }

  public update(delta: number) {
    if (!this.isMoving) return;

    const movement: IVector2 = Vector2.scale(
      Vector2.normalized(this.direction),
      this.paddle.speed * delta
    );

    if (Vector2.magnitude(movement) > 0) {
      this.paddle.position = Vector2.add(this.paddle.position, movement);

      if (this.paddle.position.y <= 0) {
        this.paddle.position.y = 0;
      } else if (
        this.paddle.position.y + this.paddle.height >=
        this.screen.height
      ) {
        this.paddle.position.y = this.screen.height - this.paddle.height;
      }
    }
  }

  public startMoving(dir: IVector2) {
    this.isMoving = true;
    this.direction = dir;
  }

  public stopMoving() {
    this.isMoving = false;
  }
}
