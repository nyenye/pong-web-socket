import { Socket } from "socket.io";
import { IBall, IVector2, IGameScreen, IPaddle } from "@pongws/types";
import { Vector2 } from "./vector2";

export class Ball implements IBall {
  private sockets: Array<Socket>;

  public position: IVector2;

  public radius: number;

  public speed: number;

  private direction: IVector2 = { x: 0, y: 0 };

  constructor(screen: IGameScreen, sockets: Array<Socket>) {
    this.sockets = sockets;
    this.radius = 5;
    this.position = {
      x: screen.width / 2 - this.radius,
      y: screen.height / 2 - this.radius
    };
    this.speed = 300;

    this.toIBall = this.toIBall.bind(this);
    this.update = this.update.bind(this);
    this.hasCollidedAABB = this.hasCollidedAABB.bind(this);
    this.rebound = this.rebound.bind(this);
    this.reset = this.reset.bind(this);

    this.reset(screen, { x: -1, y: 1 });
  }

  public toIBall(): IBall {
    return { position: this.position, radius: this.radius, speed: this.speed };
  }

  public update(delta: number): void {
    const movement: IVector2 = Vector2.scale(
      Vector2.normalized(this.direction),
      this.speed * delta
    );

    if (Vector2.magnitude(movement) > 0) {
      this.position = Vector2.add(this.position, movement);
    }
  }

  public hasCollidedAABB(paddle: IPaddle) {
    const ax = this.position.x,
      ay = this.position.y,
      aw = this.radius * 2,
      ah = this.radius * 2;

    const bx = paddle.position.x,
      by = paddle.position.y,
      bw = paddle.width,
      bh = paddle.height;

    return ax < bx + bw && ax + aw > bx && ay < by + bh && ay + ah > by;
  }

  public rebound(
    scaleX: number,
    scaleY: number,
    increaseSpeed: boolean = false
  ) {
    this.direction.x *= scaleX;
    this.direction.y *= scaleY;

    if (increaseSpeed) {
      this.speed += 10;
    }
  }

  public reset(screen: IGameScreen, direction: IVector2) {
    this.speed = 300;
    this.position = {
      x: screen.width / 2 - this.radius,
      y: screen.height / 2 - this.radius
    };
    this.direction = { x: 0, y: 0 };

    setTimeout(() => (this.direction = direction), 2000);
  }
}
