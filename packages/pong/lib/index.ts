// Sockets
import { Socket } from "socket.io";
// Types
import {
  IGameScreen,
  IPaddle,
  CourtSide,
  Scoreboard,
  EventGameStartPayload,
  EventGameLoopUpdatePayload,
  EventScoreUpdatedPayload,
  EventGameEndPayload
} from "@pongws/types";
import { Ball } from "./ball";
import { Player } from "./player";
// Events
import {
  E_GAME_END,
  E_SCOREBOARD_UPDATED,
  E_GAME_START,
  E_GAME_LOOP_UPDATE
} from "@pongws/ws-messaging";

export default class PongGame {
  private sockets: Array<Socket> = [];

  private screen: IGameScreen;

  public ball: Ball;

  public players: [Player, Player];

  public isOver: boolean = false;

  private onFinished: Function;

  constructor(
    sockets: Array<Socket>,
    screen: IGameScreen,
    onFinished: Function
  ) {
    if (sockets.length !== 2) {
      throw Error("Could not initialize game. Must have exactly 2 players");
    }
    this.sockets = sockets;
    this.screen = screen;

    this.ball = new Ball(screen, sockets);

    this.players = this.sockets.map(
      (socket: Socket, index: number): Player => {
        const side: number = index === 0 ? CourtSide.LEFT : CourtSide.RIGHT;

        const paddle: IPaddle = {
          position: {
            x: side === CourtSide.LEFT ? 10 : screen.width - 20,
            y: screen.height / 2 - 25
          },
          height: 100,
          width: 10,
          speed: 300
        };

        return new Player(socket, paddle, side, screen);
      }
    ) as [Player, Player];

    this.onFinished = onFinished;

    sockets.forEach((socket: Socket, index: number) => {
      const eventGameStartPayload: EventGameStartPayload = {
        screen,
        ball: this.ball.toIBall(),
        paddles: this.paddles,
        player: this.players[index].toIPlayer()
      };
      socket.emit(E_GAME_START, eventGameStartPayload);

      const eventGameLoopUpdatePayload: EventGameLoopUpdatePayload = {
        ball: this.ball.toIBall(),
        paddles: this.paddles
      };
    });

    this.update = this.update.bind(this);
    this.broadcast = this.broadcast.bind(this);
    this.score = this.score.bind(this);
  }

  get scoreboard(): Scoreboard {
    return [this.players[0].score, this.players[1].score];
  }

  get paddles(): [IPaddle, IPaddle] {
    return [this.players[0].paddle, this.players[1].paddle];
  }

  public update(delta: number) {
    if (this.isOver) return;

    this.ball.update(delta);
    this.players.forEach((player: Player) => player.update(delta));

    const eventGameLoopUpdatePayload: EventGameLoopUpdatePayload = {
      ball: this.ball.toIBall(),
      paddles: this.paddles
    };

    this.broadcast(E_GAME_LOOP_UPDATE, eventGameLoopUpdatePayload);

    if (
      this.ball.hasCollidedAABB(this.players[0].paddle) ||
      this.ball.hasCollidedAABB(this.players[1].paddle)
    ) {
      this.ball.rebound(-1, 1, true);
    } else if (
      this.ball.position.y <= 0 ||
      this.ball.position.y >= this.screen.height - this.ball.radius * 2
    ) {
      this.ball.rebound(1, -1);
    }

    if (this.ball.position.x <= 0) {
      this.score(this.players[1]);
      this.ball.reset(this.screen, { x: 1, y: -1 });
    } else if (
      this.ball.position.x >=
      this.screen.width - this.ball.radius * 2
    ) {
      this.score(this.players[0]);
      this.ball.reset(this.screen, { x: -1, y: 1 });
    }

    const scoreboard = this.scoreboard;

    if (scoreboard[0] >= 2 || scoreboard[1] >= 2) {
      this.isOver = true;

      const eventGameEndPayload: EventGameEndPayload = {
        scoreboard: this.scoreboard
      };
      this.broadcast(E_GAME_END, eventGameEndPayload);
      this.onFinished(this);
    }
  }

  private broadcast(event: string, payload: any) {
    this.sockets.forEach((socket: Socket) => {
      socket.emit(event, payload);
    });
  }

  private score(player: Player) {
    player.score += 1;

    const eventScoreUpdatedPayload: EventScoreUpdatedPayload = {
      scoreboard: this.scoreboard
    };
    this.sockets.forEach((socket: Socket) => {
      socket.emit(E_SCOREBOARD_UPDATED, eventScoreUpdatedPayload);
    });
  }
}

export { PongGame };
