import { IVector2 } from "@pongws/types";

export class Vector2 {
  public static add(a: IVector2, b: IVector2): IVector2 {
    return { x: a.x + b.x, y: a.y + b.y };
  }

  public static sub(a: IVector2, b: IVector2): IVector2 {
    return { x: a.x - b.x, y: a.y - b.y };
  }

  public static magnitude(vec2: IVector2): number {
    return Math.sqrt(vec2.x * vec2.x + vec2.y * vec2.y);
  }

  public static normalized(vec2: IVector2): IVector2 {
    const magnitude = Vector2.magnitude(vec2);

    if (magnitude === 0) return { x: 0, y: 0 };

    return { x: vec2.x / magnitude, y: vec2.y / magnitude };
  }

  public static distance(origin: IVector2, target: IVector2): IVector2 {
    return { x: origin.x - target.x, y: origin.y - target.y };
  }

  public static scale(vec2: IVector2, scalar: number): IVector2 {
    return { x: vec2.x * scalar, y: vec2.y * scalar };
  }
}
