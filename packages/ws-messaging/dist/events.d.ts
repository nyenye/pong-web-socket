export declare const E_MESSAGE = "E_MESSAGE";
export interface EventMessagePayload {
    id: string;
    message: string;
}
