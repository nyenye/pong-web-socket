export interface Position {
    x: number;
    y: number;
}
export interface Ball {
    position: Position;
    speed: number;
    readonly radius: number;
}
export interface Paddle {
    position: Position;
    speed: number;
    readonly height: number;
    readonly width: number;
}
