export declare const E_USER_HAS_CONNECTED = "E_USER_HAS_CONNECTED";
export declare const E_GAME_START = "E_GAME_START";
export declare const E_GAME_END = "E_GAME_END";
export declare const E_SCOREBOARD_UPDATED = "E_SCOREBOARD_UPDATED";
export declare const E_PLAYER_READY = "E_PLAYER_READY";
export declare const E_PADDLE_START_MOVING = "E_PADDLE_START_MOVING";
export declare const E_PADDLE_STOP_MOVING = "E_PADDLE_STOP_MOVING";
export declare const E_GAME_LOOP_UPDATE = "E_GAME_LOOP_UPDATE";
