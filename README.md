# Pong WebSocket Game

A demostrational multiplayer Pong Game developed using WebSocket techonogy. Vue.js is being used as the rendering layer even thought it's not a good fit. Expect rendering artifacts. 

## Screenshots

![screenshot 1](./screenshot-1.jpg)

![screenshot 2](./screenshot-2.jpg)

![screenshot 3](./screenshot-3.jpg)

![screenshot 4](./screenshot-4.jpg)

## About the project structure

It uses Lerna to structure project within individual monorepos, to better reuse code between client and server.

Check `packages/client` for the Vue.js related code.

Check `packages/pong` for the code related to the game.

Check `packages/server` for the code related to communication and matchmaking handling.

Check `packages/ws-messaging` for the code related to WebSocket event types.

Check `packages/types` for the code related to interfaces reused across all packages.